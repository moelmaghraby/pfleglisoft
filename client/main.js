import {Meteor} from 'meteor/meteor'
import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css'

import App from './App.vue';
import CompanyStats from './componenets/Company-stats.vue'
import VueMeteorTracker from 'vue-meteor-tracker'

Meteor.startup(()=>{
  Vue.use(VueMeteorTracker);
  Vue.use(Vuetify,{
    theme: {
      primary: '#FA7373',
      secondary: '#b0bec5',
      accent: '#8c9eff',
      error: '#b71c1c'
    }
  });
  new Vue(App).$mount(document.body);
  Vue.component('company-stats',CompanyStats);
})